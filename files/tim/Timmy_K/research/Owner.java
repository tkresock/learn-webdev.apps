public class Owner {
   Dog clifford;
   Cat garfield;
   
  public Owner( ) {
   System.out.println( "an Owner object was created" ); 
   clifford = new Dog( "Clifford" );
   garfield = new Cat( );
  }
 
 public static void main(String[] args) {
     Owner bill = new Owner( );
	 bill.clifford.speak( );
	 bill.clifford.color = "Green";
	 System.out.println( bill.clifford.name + "is" +bill.clifford.color);
  }
} 
